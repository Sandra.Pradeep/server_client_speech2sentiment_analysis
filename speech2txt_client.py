#!/usr/bin/env python

import sys
import rospy
from speech2txt_srv.srv import *
import speech_recognition as sr


def speech_client(y):
#convenience method that blocks until the service named analysis is available.
    rospy.wait_for_service('analysis')
    try:
        #create a handle for calling the service: 
        analysis = rospy.ServiceProxy('analysis', Speech)
        #calling handle
        resp1 = analysis(y)
        return resp1.analysis
    except rospy.ServiceException, e:
        print "Service call failed: %s"%e

# main function starts
if __name__ == "__main__":
  while 1:   
    r = sr.Recognizer()
#make previously recorded audio file as source
    with sr.WavFile("Recording_3.wav") as source:
         audio=r.record(source)
#Audio is converted into text
    y=r.recognize_google(audio)
#print the analyzed result from server
    print speech_client(y)
